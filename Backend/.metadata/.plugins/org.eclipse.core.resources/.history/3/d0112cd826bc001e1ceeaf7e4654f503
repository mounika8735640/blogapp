package com.dao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.User;

@Service
public class UserDao {

    @Autowired
    private UserRepository userRepository;

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUserById(int userId) {
        return userRepository.findById(userId).orElse(null);
    }

    public User getUserByName(String userName) {
        return userRepository.findByName(userName);
    }

    public User addUser(User user) {
        try {
            BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
            String encryptedPwd = bcrypt.encode(user.getPassword());
            user.setPassword(encryptedPwd);

            User savedUser = userRepository.save(user);

            // Log user information for debugging
            System.out.println("User added successfully: " + savedUser);

            return savedUser;
        } catch (DataIntegrityViolationException e) {
            // Log the exception for debugging purposes
            System.err.println("Error adding user: " + e.getMessage());
            e.printStackTrace();
            return null; // or throw a custom exception if needed
        }
    }




    public User updateUser(User user) {
        return userRepository.save(user);
    }

    public void deleteUserById(int userId) {
        userRepository.deleteById(userId);
    }

    public Map<String, Object> userLogin(String email, String password) {
        User dbUser = userRepository.findByEmail(email);

        Map<String, Object> result = new HashMap<>();

        if (dbUser != null) {
            BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();

            // Check if the entered password matches the stored hash
            if (bcrypt.matches(password, dbUser.getPassword())) {
                // Passwords match, populate the result map with user details
                result.put("userId", dbUser.getUserId());
                result.put("email", dbUser.getEmail());
                result.put("mobileNumber", dbUser.getMobileNumber());
                result.put("password", dbUser.getPassword());
                result.put("profilePicture", dbUser.getProfilePicture());
                result.put("userName", dbUser.getUsername());
                result.put("postId", dbUser.getPostId());

               

                return result;
            }
        }

        // User not found or passwords don't match
        result.put("error", "Invalid credentials");
        return result;
    }

}
