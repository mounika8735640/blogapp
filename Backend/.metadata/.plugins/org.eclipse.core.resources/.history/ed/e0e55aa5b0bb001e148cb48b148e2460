package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "User")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;

	@NotBlank
	@Size(min = 3, max = 50)
	@Column(unique = true, nullable = false)
	private String username;

	@NotBlank
	@Size(min = 10, max = 15)
	@Pattern(regexp = "^(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+=]).*$", message = "Password must contain one uppercase letter, one numeric digit, and one special character.")
	@Column(nullable = false)
	private String password;

	
	@Column(unique = true, nullable = false)
	@NotBlank
	@Email(message = "Email should be valid and should end with @gmail.com")
	@Pattern(regexp = ".*@gmail\\.com", message = "Email should end with @gmail.com")
	private String email;

	@Column(unique = true, nullable = false)
	@Size(min = 10, max = 10)
	@Pattern(regexp = "\\d{10}", message = "Mobile number must be 10 digits")
	private Long mobileNumber;
	
	private String profilePicture;

	@ManyToOne
	@JoinColumn(name = "postId")
	 private Post post;

	public User() {
//Default constructor
	}
	// Parameterized constructor without userId

	public User(String username, String password, String email, Long mobileNumber,
			String profilePicture) {

		this.username = username;
		this.password = password;
		this.email = email;
		this.mobileNumber = mobileNumber;
		this.profilePicture = profilePicture;
	}

	public User(int userId, String username, String password, String email, Long mobileNumber,
			String profilePicture) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.email = email;
		this.mobileNumber = mobileNumber;
		this.profilePicture = profilePicture;

	}

	// Getters for Post variable
	public Post getPost() {
		return post;
	}

	// Setters for Post varibale
	public void setPost(Post post) {
		this.post = post;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}
}
