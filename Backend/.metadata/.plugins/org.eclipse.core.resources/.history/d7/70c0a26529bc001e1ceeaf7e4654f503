package com.dao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.User;

@Service
public class UserDao {

    @Autowired
    private UserRepository userRepository;

    @Autowired
	private JavaMailSender mailSender;


    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUserById(int userId) {
        return userRepository.findById(userId).orElse(null);
    }

    public User getUserByName(String userName) {
        return userRepository.findByName(userName);
    }

    public User addUser(User user) {
        try {
            BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
            String encryptedPwd = bcrypt.encode(user.getPassword());
            user.setPassword(encryptedPwd);

            User savedUser = userRepository.save(user);

            // Log user information for debugging
            System.out.println("User added successfully: " + savedUser);

            sendWelcomeEmail(savedUser);

            return savedUser;
            private void sendWelcomeEmail(User user) {
        		
        		SimpleMailMessage message = new SimpleMailMessage();
        		message.setTo(user.getEmail());
        		message.setSubject("Welcome to our website");
        		message.setText("Dear " + user.getUserName() + ",\n\n"
        				+ "Thank you for registering ");

        		mailSender.send(message);
        	}

        } catch (DataIntegrityViolationException e) {
            System.err.println("Error adding user: " + e.getMessage());
            e.printStackTrace();
            return null; 
        }
    }




    private void sendWelcomeEmail(User savedUser) {
		// TODO Auto-generated method stub
		
	}

	public User updateUser(User user) {
        return userRepository.save(user);
    }

    public void deleteUserById(int userId) {
        userRepository.deleteById(userId);
    }

    public Map<String, Object> userLogin(String email, String password) {
        User dbUser = userRepository.findByEmail(email);

        Map<String, Object> result = new HashMap<>();

        if (dbUser != null) {
            BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();

            if (bcrypt.matches(password, dbUser.getPassword())) {
              
                result.put("userId", dbUser.getUserId());
                result.put("email", dbUser.getEmail());
                result.put("mobileNumber", dbUser.getMobileNumber());
                result.put("password", dbUser.getPassword());
                result.put("profilePicture", dbUser.getProfilePicture());
                result.put("userName", dbUser.getUsername());
                result.put("postId", dbUser.getPost());
                
                return result;
            }
        }
        result.put("error", "Invalid credentials");
        return result;
    }

}
